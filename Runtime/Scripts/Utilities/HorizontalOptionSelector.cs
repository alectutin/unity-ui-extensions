using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Utilities
{
    [ExecuteAlways]
    public class HorizontalOptionSelector : MonoBehaviour
    {

        [System.Serializable]
        public class HorizontalOptionSelectorUpdatedEvent : UnityEvent<int>
        {
        }

        [SerializeField] public Button leftButton;
        
        [SerializeField] public Button rightButton;
        
        [SerializeField] public RectTransform content;
        
        [SerializeField] public List<RectTransform> options;

        [SerializeField] public int displayedIndex = 0;
        
        [SerializeField] public float animationSpeedMultiplier = 10.0f;

        [SerializeField] public HorizontalOptionSelectorUpdatedEvent onValueChanged;

        [SerializeField] private bool triggerValueChangeAfterAnimation = false;

        private bool nextChangeIsNotify = false;

        private float currentDisplayedIndex = 0.0f;

        private int internalIndex;

        public bool CurrentlyAnimating => Mathf.Abs(currentDisplayedIndex - displayedIndex) > 0.01f;

        private void Start()
        {
            leftButton.onClick.AddListener(OnLeftClicked);
            rightButton.onClick.AddListener(OnRightClicked);
            SetCurrentValue(displayedIndex, false);
            UpdateButtons();
        }

        private void OnLeftClicked()
        {
            SetCurrentValue(displayedIndex - 1);
        }
        
        private void OnRightClicked()
        {
            SetCurrentValue(displayedIndex + 1);
        }

        public void SetCurrentValue(int newValue, bool notify = true)
        {
            nextChangeIsNotify = notify;
            newValue = Mathf.Clamp(newValue, 0, options.Count - 1);
            if (newValue != internalIndex || newValue != displayedIndex)
            {
                displayedIndex = newValue;
                internalIndex = newValue;
                if (!triggerValueChangeAfterAnimation)
                {
                    NotifyIfApplicable();
                }

                UpdateButtons();
            }
        }

        private void NotifyIfApplicable()
        {
            if (nextChangeIsNotify && Application.isPlaying)
            {
                onValueChanged.Invoke(internalIndex);
            }
        }

        private void UpdateButtons()
        {
            if (leftButton != null)
            {
                leftButton.interactable = internalIndex > 0;
            }
            if (rightButton != null)
            {
                rightButton.interactable = internalIndex < (options.Count - 1);
            }
        }

        private void Update()
        {
            //Allows changing in the editor.
            SetCurrentValue(displayedIndex);

            if (!CurrentlyAnimating)
            {
                return;
            }

            if (Application.isPlaying)
            {
                //TODO - Curves & Animation.
                currentDisplayedIndex = Mathf.Lerp(currentDisplayedIndex, displayedIndex, Mathf.Clamp(animationSpeedMultiplier * Time.deltaTime, 0f, 0.5f));
            }
            else
            {
                //Instant snap for debugging.
                currentDisplayedIndex = displayedIndex;
            }
            
            if (!CurrentlyAnimating)
            {
                //Snap to the end and stop animating.
                currentDisplayedIndex = displayedIndex;
                if (triggerValueChangeAfterAnimation)
                {
                    NotifyIfApplicable();
                }
            }

            float currentDisplayOffsetX = 0.0f;
            int fullCount = Mathf.FloorToInt(currentDisplayedIndex);
            
            int displayElementIndex;
            for (displayElementIndex = 0; displayElementIndex < fullCount; displayElementIndex++)
            {
                currentDisplayOffsetX -= options[displayElementIndex].rect.width;
            }

            if (displayElementIndex < options.Count)
            {
                float percentageMultiplier = currentDisplayedIndex % 1.0f;
                currentDisplayOffsetX -= percentageMultiplier * options[displayElementIndex].rect.width;
            }

#if UNITY_EDITOR
            if (!Application.isPlaying && content == null)
            {
                return;
            }
#endif
            //Update the position.
            Vector2 anchoredPosition = content.anchoredPosition;
            anchoredPosition.x = currentDisplayOffsetX;
            content.anchoredPosition = anchoredPosition;

        }
    }
}